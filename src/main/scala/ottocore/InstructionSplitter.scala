package ottocore

import spinal.core._

/**
 * Simple rewiring to split instruction into parts like `opcode`, `rd`, `rs1`, `rs2`, ...
 * No logic involved.
 */
class InstructionSplitter extends Component {

  val XLEN = 32 bits // register size

  val io = new Bundle {
    val instruction = in Bits (XLEN)

    val opcode = out Bits (7 bits)
    val func3 = out Bits (3 bits)
    val func7 = out Bits (7 bits)

    val rd = out Bits (5 bits)
    val rs1 = out Bits (5 bits)
    val rs2 = out Bits (5 bits)


    val imm_i_type = out Bits (12 bits)
    val imm_s_type = out Bits (12 bits)
    val imm_u_type = out Bits (20 bits)
    val imm_sb_type = out Bits (13 bits)
    val imm_j_type = out Bits(21 bits)
  }

  val i = io.instruction

  // Opcodes
  io.opcode := i(6 downto 0)
  io.func3 := i(14 downto 12)
  io.func7 := i(31 downto 25)

  // Register addresses.
  io.rd := i(11 downto 7)
  io.rs1 := i(19 downto 15)
  io.rs2 := i(24 downto 20)

  // Immediate values.
  io.imm_i_type := i(31 downto 20)
  io.imm_u_type := i(31 downto 12)
  io.imm_s_type := i(31 downto 25) ## i(11 downto 7)

  io.imm_sb_type := i(31) ## i(7) ## i(30 downto 25) ## i(11 downto 8) ## B"0"

  io.imm_j_type := i(31) ## i(19 downto 12) ## i(20) ## i(30 downto 21) ## B"0"

}
