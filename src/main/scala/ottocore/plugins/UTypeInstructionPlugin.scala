package ottocore.plugins

import ottocore._
import spinal.core._

/**
 * Implements AUIPC and LUI instructions.
 */
class UTypeInstructionPlugin extends Plugin {

  override def build(core: Attocore): Unit = {

    val alu = core.findPlugins(classOf[IntAluPlugin]).head

    def default_u_type_actions(): Unit = {

      core.aluSrcB := AluSrcB.IMM_U
      core.aluCtrl := AluCtrl.AND

      // Write result of ALU operation into register.
      core.rdWData := alu.alu.io.rsp.result

      // Handle illegal register addresses.
      when(core.illegalRDAddr) {
        core.raiseIllegalInstructionException()
      } otherwise {
        core.writeBackRegisterEnable := True
        core.commitInstruction()
      }
    }

    val dec = core.getDecoderService()

    import Instructions._
    // -- U-Type --

    dec.registerInstruction(
      LUI,
      core => {
        // Load Upper Immediate.
        // Add zero to IMM_U, this way IMM_U is pushed through the ALU without being modified,
        // and we save some multiplexers.
        default_u_type_actions()
        // Add 0 to immediate.
        core.rs1_addr := U(0)
        core.aluSrcA := AluSrcA.RS1
      }
    )
    dec.registerInstruction(
      AUIPC,
      core => {
        // RD = PC + (IMM_U << 12)
        // Add zero to IMM_U
        default_u_type_actions()
        core.aluSrcA := AluSrcA.PC
      }
    )

  }

}