package ottocore.plugins

import spinal.core.GenerationFlags.formal
import spinal.core._

/**
 * @param numRegisters       Number of registers including r0. Must be 16 or 32.
 * @param XLEN               Bit width of registers.
 * @param numFormalRegisters Number of active registers for formal checks (including r0).
 * @param resetToZero        true: Register file is set to all zeros on reset, false: register file data is undefined.
 *                           Allowing the registers to have undefined state might save some area because registers without reset/preload
 *                           might be used.
 */
class RegisterFile(numRegisters: Int = 16,
                   XLEN: BitCount = 32 bits,
                   numFormalRegisters: Int = 3,
                   resetToZero: Boolean = true
                  ) extends Component {

  assert(numFormalRegisters >= 3, "Less registers than x0, x1, x2 do not make sense.")
  assert(numFormalRegisters <= numRegisters, "Cannot have more formal registers than real ones.")
  assert(numRegisters == 16 | numRegisters == 32, "Expect to have 16 or 32 registers.")

  val REG_ADDR_LEN = log2Up(numRegisters) bits

  val io = new Bundle {
    val r_addr1 = in UInt (REG_ADDR_LEN)
    val r_data1 = out Bits (XLEN)

    val r_addr2 = in UInt (REG_ADDR_LEN)
    val r_data2 = out Bits (XLEN)

    val w_addr = in UInt (REG_ADDR_LEN)
    val w_data = in Bits (XLEN)
    val w_valid = in Bool
  }

  // For formal checks:
  // Disable all registers except r1 and r2.
  // This should speed up formal checks without
  // having an impact the results.
  formal {
    assume(io.w_addr < numFormalRegisters)
    assume(io.r_addr1 < numFormalRegisters)
    assume(io.r_addr2 < numFormalRegisters)
  }


  //  val r_strobe1 = Bits(NUM_REGISTERS bits)
  //  r_strobe1(0) := False // Strobe to register 0 is always False.
  //  (1 until NUM_REGISTERS)
  //    .foreach(i => r_strobe1(i) := io.r_addr1 === U(i))
  //
  //  val r_strobe2 = Bits(NUM_REGISTERS bits)
  //  r_strobe2(0) := False // Strobe to register 0 is always False.
  //  (1 until NUM_REGISTERS)
  //    .foreach(i => r_strobe2(i) := io.r_addr2 === U(i))

  val w_addr = UInt(REG_ADDR_LEN)
  w_addr := U(0)
  when(io.w_valid) {
    w_addr := io.w_addr
  }


  // Create registers.
  val registers = (1 until numRegisters)
    .map(i => {
      // Write condition:
      val writeEnable = w_addr === U(i)
      val reg = RegNextWhen(io.w_data, writeEnable)
      if (resetToZero)
        // Reset to zero on reset if desired.
        reg.init(B(0))
      reg
    })


  val register_vec = Vec(List(B(0, 32 bits)) ++ registers)
  assert(register_vec.length == numRegisters)
  assert(register_vec(0).getWidth == XLEN.value)

  // Find output data for read port 1.
  io.r_data1 := register_vec(io.r_addr1)

  // Find output data for read port 2.
  io.r_data2 := register_vec(io.r_addr2)


}
