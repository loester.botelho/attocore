package ottocore.plugins

import ottocore._
import spinal.core.GenerationFlags._
import spinal.core._
import spinal.lib._

object AluCtrl extends SpinalEnum {
  val NOP, ADD, SUB, XOR, OR, AND, SetLessThan, SetLessThanU = newElement() // TODO: remove NOP
}

object AddSubCtrl extends SpinalEnum {
  val Add, Sub = newElement()
}

case class AluCmd(XLEN: BitCount = 32 bits) extends Bundle {
  val operand_a = Bits(XLEN)
  val operand_b = Bits(XLEN)
  val control = AluCtrl()
}

case class AluRsp(XLEN: BitCount = 32 bits) extends Bundle {
  val result = Bits(XLEN)
  val resultIsZero = Bool()
  val resultIsNegative = Bool()
  val aEqualsB = Bool()
  val aLessThanB = Bool()
}

class IntAlu(XLEN: BitCount = 32 bits) extends Component {

  val io = new Bundle {
    val cmd = slave Stream AluCmd(XLEN)
    val rsp = master Stream AluRsp(XLEN)
  }

  // Cast operands to UInt.
  val operand_a_uint = io.cmd.operand_a.asUInt
  val operand_b_uint = io.cmd.operand_b.asUInt

  val a = operand_a_uint
  val b = operand_b_uint

  // Bitwise operations.
  val xor = (a ^ b).asBits
  val or = (a | b).asBits
  val and = (a & b).asBits

  // Addition and subtraction.
  // Combine addition and subtraction into one.
  // Conditionally negate (two-complement) b before addition.
  // The +1 can be implemented using the carry-in of the first full-adder.
  val doSubtraction = False // or True, False == Add, True == Sub
  // Create a mask of all 0s or 1s.
  val xorMask = Cat((0 until XLEN.value).map(_ => doSubtraction.asBits))
  val b_maybeneg = (b.asBits ^ xorMask).asUInt + U(doSubtraction)
  val addSub = (a + b_maybeneg).asBits

  formal {
    when(doSubtraction) {
      assert(addSub === (a - b).asBits, "Adder-subtractor should implement subtraction.")
    } otherwise {
      assert(addSub === (a + b).asBits, "Adder-subtractor should implement addition.")
    }
  }

  val result = Bits(XLEN)

  // is a < b ?
  //  val lessUnsigned = Mux(a.msb === b.msb, sub.msb, b.msb)
  //  val lessSigned = Mux(a.msb === b.msb, sub.msb, a.msb)
  //  val less = Mux(useLessUnsigned, lessUnsigned, lessSigned)

  // Compute (a < b).
  val unsignedLess = False
  val relevant_msb = Mux(unsignedLess, b.msb, a.msb)
  val aLessThanB = Mux(a.msb === b.msb, addSub.msb, relevant_msb)

  formal {
    // Assert that `aLessThanB` is correct.
    when(doSubtraction) {
      when(unsignedLess) {
        assert(aLessThanB === (a < b))
      } otherwise {
        assert(aLessThanB === (a.asSInt < b.asSInt))
      }
    }
  }

  val addSubIsZero = !addSub.orR

  val setIfLessThan = B(aLessThanB).resize(XLEN)


  switch(io.cmd.control) {
    import AluCtrl._
    is(NOP) {
      result := B(0)
    }
    is(ADD) {
      doSubtraction := False
      result := addSub
      assert(result === (a + b).asBits)
    }
    is(SUB) {
      doSubtraction := True
      result := addSub
      assert(result === (a - b).asBits)
    }
    is(XOR) {
      result := xor
      assert(result === (a ^ b).asBits)
    }
    is(OR) {
      result := or
      assert(result === (a | b).asBits)
    }
    is(AND) {
      result := and
      assert(result === (a & b).asBits)
    }
    is(SetLessThan) {
      doSubtraction := True
      unsignedLess := False
      result := setIfLessThan
      assert(result === Mux(a.asSInt < b.asSInt, B(1), B(0)).resized)
    }
    is(SetLessThanU) {
      doSubtraction := True
      unsignedLess := True
      result := setIfLessThan
      assert(result === Mux(a < b, B(1), B(0)).resized)
    }
  }

  val resultBundle = AluRsp(XLEN)
  resultBundle.result := result
  resultBundle.resultIsNegative := result(XLEN.value - 1)
  resultBundle.resultIsZero := addSubIsZero
  resultBundle.aEqualsB := addSubIsZero
  resultBundle.aLessThanB := aLessThanB
  io.rsp << io.cmd.translateWith(resultBundle)

}

class IntAluPlugin extends Plugin {

  var alu: IntAlu = null

  override def setup(core: Attocore): Unit = {
    alu = new IntAlu()
  }

  override def build(core: Attocore): Unit = {

    // Wire up the ALU.
    alu.io.cmd.valid := True
    alu.io.rsp.ready := True

    alu.io.cmd.operand_a := core.aluOperandA
    alu.io.cmd.operand_b := core.aluOperandB

    alu.io.cmd.control := AluCtrl.ADD

    core.aluResult := alu.io.rsp.result
    core.aluResultEquals := alu.io.rsp.aEqualsB
    core.aluResultLess := alu.io.rsp.aLessThanB

    // Register instructions.
    val dec = core.getDecoderService()

    def default_r_type_actions(): Unit = {
      core.aluSrcA := AluSrcA.RS1
      core.aluSrcB := AluSrcB.RS2
//      core.rd_source := RDSrc.AluOutput
      core.rdWData := alu.io.rsp.result

      // Handle illegal register addresses.
      when(core.illegalRS1Addr | core.illegalRS2Addr | core.illegalRDAddr) {
        core.raiseIllegalInstructionException()
      } otherwise {
        core.writeBackRegisterEnable := True
      }
      core.commitInstruction()
    }

    def default_i_type_actions(signExtend: Boolean = true): Unit = {
      core.aluSrcA := AluSrcA.RS1
      if (signExtend) {
        core.aluSrcB := AluSrcB.IMM_I_S
      } else {
        core.aluSrcB := AluSrcB.IMM_I_U
      }
//      core.rd_source := RDSrc.AluOutput
      core.rdWData := alu.io.rsp.result

      // Handle illegal register addresses.

      when(core.illegalRS1Addr | core.illegalRDAddr) {
        core.raiseIllegalInstructionException()
      } otherwise {
        core.writeBackRegisterEnable := True
      }

      core.commitInstruction()
    }

    val aluCtrl = alu.io.cmd.control
    import Instructions._
    dec.registerInstruction(
      ADD,
      core => {
        aluCtrl := AluCtrl.ADD
        default_r_type_actions()
      }
    )
    dec.registerInstruction(
      SUB,
      core => {
        aluCtrl := AluCtrl.SUB
        default_r_type_actions()
      }
    )
    dec.registerInstruction(
      XOR,
      core => {
        aluCtrl := AluCtrl.XOR
        default_r_type_actions()
      }
    )
    dec.registerInstruction(
      OR,
      core => {
        aluCtrl := AluCtrl.OR
        default_r_type_actions()
      }
    )
    dec.registerInstruction(
      AND,
      core => {
        aluCtrl := AluCtrl.AND
        default_r_type_actions()
      }
    )
    dec.registerInstruction(
      SLT,
      core => {
        aluCtrl := AluCtrl.SetLessThan
        default_r_type_actions()
      }
    )
    dec.registerInstruction(
      SLTU,
      core => {
        aluCtrl := AluCtrl.SetLessThanU
        default_r_type_actions()
      }
    )
    // -- I-Type --
    dec.registerInstruction(
      ADDI,
      core => {
        aluCtrl := AluCtrl.ADD
        default_i_type_actions()
      }
    )
    dec.registerInstruction(
      XORI,
      core => {
        aluCtrl := AluCtrl.XOR
        default_i_type_actions()
      }
    )
    dec.registerInstruction(
      ORI,
      core => {
        aluCtrl := AluCtrl.OR
        default_i_type_actions()
      }
    )
    dec.registerInstruction(
      ANDI,
      core => {
        aluCtrl := AluCtrl.AND
        default_i_type_actions()
      }
    )
  }
}
