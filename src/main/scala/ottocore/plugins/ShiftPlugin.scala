package ottocore.plugins

import ottocore._
import spinal.core._
import spinal.lib._

object ShifterCmd extends SpinalEnum {
  val ShiftLeft, ShiftRightZeroExtend, ShiftRightSignExtend = newElement()
}

/**
 * Shift to the right only.
 *
 * @param XLEN
 */
class ShifterRight(XLEN: BitCount) extends Component {
  val io = new Bundle {
    val cmd = slave Stream (new Bundle {
      val data = Bits(XLEN)
      val shiftAmount = UInt(log2Up(XLEN.value) bits)
      val signExtend = Bool
    })
    val rsp = master Stream Bits(XLEN)
  }
  assert(io.cmd.shiftAmount.getWidth < 32)

  /**
   * Conditional shift to the right with static shift amount and optional sign extension.
   *
   * @param shiftAmount
   * @param shiftEnable
   * @param signExtend
   * @param data
   * @return Returns the shifted data signal.
   */
  def shiftRightN(shiftAmount: Int, shiftEnable: Bool, signExtend: Bool, data: Bits): Bits = {
    assert(shiftAmount > 0)
    val shifted = data(shiftAmount until data.getWidth)
    // Create prefix which is either all zero or all the repeated MSB of data for sign extension.
    val prefix = Mux(signExtend, (0 until shiftAmount).map(_ => data.msb).asBits(), B(0))
    val shiftedAndExtended = prefix ## shifted
    Mux(shiftEnable, shiftedAndExtended, data)
  }

  // Construct barrel shifter based on static shifters.
  val shifted = io.cmd.shiftAmount.asBools.zipWithIndex
    .foldLeft(io.cmd.data) {
      case (data, (enableBit, enableBitIndex)) => {
        val shiftAmount = 1 << enableBitIndex
        shiftRightN(shiftAmount, enableBit, io.cmd.signExtend, data)
      }
    }

  io.rsp << io.cmd.translateWith(shifted)
}

/**
 * Use a shifter that can shift to the right only and sandwith it into two bit-reverse circuits.
 * This yields a shifter that can also shift to the left.
 *
 * @param XLEN
 */
class Shifter(XLEN: BitCount) extends Component {
  val io = new Bundle {
    val cmd = slave Stream (new Bundle {
      val data = Bits(XLEN)
      val shiftAmount = UInt(log2Up(XLEN.value) bits)
      val shiftCtrl = ShifterCmd()
    })
    val rsp = master Stream Bits(XLEN)
  }

  val rightShifter = new ShifterRight(XLEN)

  // Set control bits.
  val reverseBits = io.cmd.shiftCtrl === ShifterCmd.ShiftLeft
  val signExtend = io.cmd.shiftCtrl === ShifterCmd.ShiftRightSignExtend

  // Reverse the input bits if shift to the left should be computed.
  val dataInputMaybeReversed = Mux(
    reverseBits,
    io.cmd.data.asBools.reverse.asBits(),
    io.cmd.data
  )

  rightShifter.io.cmd.data := dataInputMaybeReversed
  rightShifter.io.cmd.shiftAmount := io.cmd.shiftAmount
  rightShifter.io.cmd.signExtend := signExtend

  val dataOut = rightShifter.io.rsp.payload

  // Reverse output bits again if necessary.
  val dataOutReversedBack = Mux(
    reverseBits,
    dataOut.asBools.reverse.asBits(),
    dataOut
  )

  io.rsp << rightShifter.io.rsp.translateWith(dataOutReversedBack)

}

class ShiftPlugin(XLEN: BitCount = 32 bits) extends Plugin {

  var shifter: Shifter = null

  override def setup(core: Attocore): Unit = {
    shifter = new Shifter(XLEN)
    shifter.io.cmd.valid := False
    shifter.io.rsp.ready := True
  }

  override def build(core: Attocore): Unit = {
    val dec = core.getDecoderService()

    // TODO: What happens if the shift amount is larger than 32 ???
    val shiftAmountReg = core.RS2(0 to 4).asUInt
    val shiftAmountImm = core.instructionSplitter.io.imm_i_type(0 to 4).asUInt

    shifter.io.cmd.shiftCtrl := ShifterCmd.ShiftLeft
    shifter.io.cmd.data := core.RS1
    shifter.io.cmd.shiftAmount := shiftAmountReg

    def default_shift_r_actions(): Unit = {
      shifter.io.cmd.valid := True
      shifter.io.rsp.ready := True
      core.rdWData := shifter.io.rsp.payload
      core.writeBackRegisterEnable := True
      core.commitInstruction()
    }

    def default_shift_i_actions(): Unit = {
      shifter.io.cmd.valid := True
      shifter.io.rsp.ready := True
      shifter.io.cmd.shiftAmount := shiftAmountImm
      core.rdWData := shifter.io.rsp.payload
      core.writeBackRegisterEnable := True
      core.commitInstruction()
    }

    import Instructions._
    dec.registerInstruction(
      SLL,
      core => {
        shifter.io.cmd.shiftCtrl := ShifterCmd.ShiftLeft
        default_shift_r_actions()
      }
    )
    dec.registerInstruction(
      SRL,
      core => {
        shifter.io.cmd.shiftCtrl := ShifterCmd.ShiftRightZeroExtend
        default_shift_r_actions()
      }
    )
    dec.registerInstruction(
      SRA,
      core => {
        shifter.io.cmd.shiftCtrl := ShifterCmd.ShiftRightSignExtend
        default_shift_r_actions()
      }
    )
    dec.registerInstruction(
      SLLI,
      core => {
        shifter.io.cmd.shiftCtrl := ShifterCmd.ShiftLeft
        default_shift_i_actions()
      }
    )
    dec.registerInstruction(
      SRLI,
      core => {
        shifter.io.cmd.shiftCtrl := ShifterCmd.ShiftRightZeroExtend
        default_shift_i_actions()
      }
    )
    dec.registerInstruction(
      SRAI,
      core => {
        shifter.io.cmd.shiftCtrl := ShifterCmd.ShiftRightSignExtend
        default_shift_i_actions()
      }
    )
  }

}
