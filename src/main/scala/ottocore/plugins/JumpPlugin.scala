package ottocore.plugins

import ottocore._
import spinal.core._

/**
 * Implements unconditional jump instructions (JAL, JALR).
 */
class JumpPlugin extends Plugin {

  override def build(core: Attocore): Unit = {

    val dec = core.getDecoderService()

    import Instructions._

    dec.registerInstruction(
      JAL,
      core => {
        core.aluSrcA := AluSrcA.PC
        core.aluSrcB := AluSrcB.IMM_J

        //        core.rd_source := RDSrc.PC_PLUS4
        // Store the PC+4 in the register.
        core.rdWData := core.pc_plus4.asBits

        // Set next PC to result of ALU operation.
        core.pc_next := core.aluResult.asUInt

        // Handle illegal register addresses.
        when(core.illegalRDAddr) {
          core.raiseIllegalInstructionException()
        } otherwise {
          core.writeBackRegisterEnable := True
        }

        core.commitInstruction()
      }
    )
    dec.registerInstruction(
      JALR,
      core => {
        core.aluSrcA := AluSrcA.RS1
        core.aluSrcB := AluSrcB.IMM_I_S

        // Store the PC+4 in the register.
        core.rdWData := core.pc_plus4.asBits

        // Set next PC to result of ALU operation.
        core.pc_next := core.aluResult.asUInt

        // Handle illegal register addresses.
        when(core.illegalRDAddr) {
          core.raiseIllegalInstructionException()
        } otherwise {
          core.writeBackRegisterEnable := True
        }

        core.commitInstruction()
      }
    )
  }
}
