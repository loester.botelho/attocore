package ottocore

import spinal.core.MaskedLiteral

trait Plugin {
  def setup(core: Attocore): Unit = {}

  def build(core: Attocore): Unit = {}

  def post(core: Attocore): Unit = {}
}

trait DecoderService {
  def registerInstruction(instruction: MaskedLiteral, action: Attocore => Unit): Unit

  def registerInstructions(instrs: Iterable[(MaskedLiteral, Attocore => Unit)]): Unit = {
    for ((i, a) <- instrs) {
      registerInstruction(i, a)
    }
  }
}

