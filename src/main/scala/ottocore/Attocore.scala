/// This file is part of the SpinalHDL ottocore implementation
/// (see https://codeberg.org/tok/ottocore-tok)
///
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the CERN Open Hardware License (CERN OHL-S) as it will be published
/// by the CERN, either version 2.0 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// CERN Open Hardware License for more details.
///
/// You should have received a copy of the CERN Open Hardware License
/// along with this program. If not, see <http://ohwr.org/licenses/>.
///
/// Author: Thomas Kramer <code@tkramer.ch>
/// December 2019


package ottocore

import ottocore.plugins.{AluCtrl, DummyNOPs, InstructionDecoderPlugin, IntAluPlugin, JumpPlugin, LoadStorePlugin, RegisterFilePlugin, ShiftPlugin, SingleCycleBranchPlugin, TwoCycleBranchPlugin, UTypeInstructionPlugin, WFIPlugin}
import spinal.core.GenerationFlags.formal
import spinal.core.Formal._
import spinal.core._
import spinal.lib._
import spinal.lib.LatencyAnalysis
import spinal.lib.fsm._


// Tell what to put into operand A of the ALU.
object AluSrcA extends SpinalEnum {
  val PC, RS1 = newElement()
}

// Tell what to put into operand B of the ALU.
object AluSrcB extends SpinalEnum {
  val RS2, IMM_I_U, IMM_I_S, IMM_S_S, IMM_U, IMM_B, IMM_J = newElement()
}


/**
 * Top-level of Ottocore implementation.
 *
 * See: https://codeberg.org/ottocore/ottocore-framework
 */
class Attocore(
                plugins: List[Plugin]
              ) extends Component {

  val XLEN = 32 bits // register size
  val DEFAULT_BOOT_ADDR = U"x8000_0000"

  val NUM_REGISTERS = 16
  val REG_ADDR_LEN = log2Up(NUM_REGISTERS) bits

  val io = new Bundle {
    // status signals
    val clk_i = in Bool
    val rst_ni = in Bool

    // launch core
    val wakeup_i = in Bool
    val wakeup_resume_i = in Bool
    val wakeup_addr_i = in UInt (XLEN)

    // exception handling
    val exc_valid_o = out Bool
    val exc_cause_o = out UInt (XLEN)

    // instruction bus
    val i_req_addr_o = out UInt (XLEN)
    val i_req_ready_i = in Bool
    val i_req_valid_o = out Bool // address is valid

    val i_rsp_data_i = in Bits (XLEN)
    val i_rsp_valid_i = in Bool // data is ready
    val i_rsp_ready_o = out Bool

    // data bus
    val d_req_addr_o = out UInt (XLEN)
    val d_req_wdata_o = out Bits (XLEN)
    val d_req_wstrb_o = out Bits (XLEN.value / 8 bits)
    val d_req_ready_i = in Bool
    val d_req_valid_o = out Bool

    val d_rsp_rdata_i = in Bits (XLEN)
    val d_rsp_valid_i = in Bool
    val d_rsp_ready_o = out Bool
  }

  // Don't append `io_` to the verilog signals.
  noIoPrefix()

  formal {
    // Assume we start with reset HIGH.
    val reset = clockDomain.isResetActive
    when(initstate()) {
      assume(reset)
    } otherwise {
      assume(!reset)
    }
  }

  io.exc_valid_o := False
  io.exc_cause_o := U(0)

  io.i_req_valid_o := False
  io.i_rsp_ready_o := False


  // No combinational paths from inputs to outputs!
  //    assert(LatencyAnalysis(io.i_req_ready_i, io.i_req_valid_o) > 0)

  // === Signal definitions ===

  val currentInstruction = Bits(XLEN)

  // Register file addresses.
  val rs1_addr = UInt(REG_ADDR_LEN)
  val rs2_addr = UInt(REG_ADDR_LEN)
  val rd_addr = UInt(REG_ADDR_LEN)

  // Data read from rs1 and rs1 registers.
  val RS1 = Bits(XLEN)
  val RS2 = Bits(XLEN)
  // Data to be written into RD register.
  val rdWData = Bits(XLEN)

  // Write enable to RD?
  val writeBackRegisterEnable = False

  // Next value for PC.
  val pc_next = UInt(XLEN)

  // Default values.
  val exception_invalid_instruction = False
  val exception_misaligned_load_store = False
  val any_exception = Bool()
  val exception_cause = U(0, XLEN)

  def raiseIllegalInstructionException(): Unit = {
    exception_invalid_instruction := True
    exception_cause := U(Causes.illegal_instruction)
  }

  val illegalRS1Addr = Bool()
  val illegalRS2Addr = Bool()
  val illegalRDAddr = Bool()

  // Tell which registers are needed.
  //  val useRS1 = Bool()
  //  val useRS2 = Bool()
  //  val useRD = Bool()

  // By default the ALU gets commands from the instruction decoder.
  //  val aluCtrlSrc = AluCtrlSrc()
  val aluCtrl = AluCtrl()

  val aluOperandA = Bits(XLEN)
  val aluOperandB = Bits(XLEN)
  val aluResult = Bits(XLEN)
  val aluResultEquals = Bool()
  val aluResultLess = Bool()

  // Default sources for ALU operands.
  val aluSrcA = AluSrcA()
  val aluSrcB = AluSrcB()

  // By default there is no interrupt.
  val waitForInterrupt = Bool()

  // Data coming from load/store unit.
  val lsuResult = Bits(XLEN)

  // Instructions are expected to be aligned to 4 bytes, so lower 2 bits are not necessary.

  // Program counter, by default initialized to default boot address.
  // Since instructions are aligned to 4 bytes the lower two bits are not used.
  val pc_update_strb = False
  val pc = RegNextWhen(
    pc_next,
    pc_update_strb,
    init = DEFAULT_BOOT_ADDR
  )

  def updatePC(): Unit = {
    pc_update_strb := True
  }

  // Increment PC by four bytes.
  val pc_plus4 = pc + U(4)

  // Default: increment PC by four bytes.
  pc_next := pc_plus4

  // Output PC at instruction bus.
  io.i_req_addr_o := pc

  //  val instructionDecoder = new InstructionDecoder
  //  instructionDecoder.io.instruction := io.i_rsp_data_i
  // TODO: can save this register when assuming io.i_rsp_data_i to be stable (as AXI requires it).
  // Now instruction is stored in a separate register for debugging.
  val instructionReg = Reg(Bits(XLEN)) init (0)
  currentInstruction := instructionReg

  // Instruction splitter.
  val instructionSplitter = new InstructionSplitter
  instructionSplitter.io.instruction := currentInstruction


  val fetchNewInstruction = RegInit(False)
  val acceptNewInstruction = RegInit(False)
  val instructionPresent = RegInit(False)
  val instructionCommitStrobe = False

  def commitInstruction(): Unit = {
    instructionCommitStrobe := True
  }

  def getDecoderService(): DecoderService = {
    val decoderServices = plugins.filter(p => p.isInstanceOf[DecoderService])
    assert(decoderServices.length == 1, s"Expected to find exactly 1 decoder service: Found ${decoderServices.length}")
    decoderServices(0).asInstanceOf[DecoderService]
  }

  /**
   * Find other plugins by their class.
   *
   * @tparam T
   * @return
   */
  def findPlugins[T](cl: Class[T]): List[T] = {
    plugins.filter(p => cl.isAssignableFrom(p.getClass))
      .map(p => p.asInstanceOf[T])
  }

  // Run setup phase.
  for (plugin <- plugins) {
    plugin.setup(this)
  }

  // Run build phase.
  for (plugin <- plugins) {
    plugin.build(this)
  }

  // Run post phase.
  for (plugin <- plugins) {
    plugin.post(this)
  }


  // Wiring up operands to integer ALU.

  // Multiplexer for ALU operand A.
  switch(aluSrcA) {
    is(AluSrcA.RS1) {
      aluOperandA := RS1
    }
    is(AluSrcA.PC) {
      aluOperandA := pc.asBits
    }
  }

  // Multiplexer for ALU operand B.
  switch(aluSrcB) {
    is(AluSrcB.RS2) {
      aluOperandB := RS2
    }
    is(AluSrcB.IMM_I_U) {
      // No sign extension.
      aluOperandB := instructionSplitter.io.imm_i_type.asUInt.resize(XLEN).asBits
    }
    is(AluSrcB.IMM_I_S) {
      // With sign extension.
      aluOperandB := instructionSplitter.io.imm_i_type.asSInt.resize(XLEN).asBits
    }
    is(AluSrcB.IMM_S_S) {
      // Immediate value used in store operations.
      aluOperandB := instructionSplitter.io.imm_s_type.asSInt.resize(XLEN).asBits
    }
    is(AluSrcB.IMM_U) {
      // Immediate value used for LUI (load upper immediate).
      aluOperandB := instructionSplitter.io.imm_u_type ## B(0, 12 bits)
    }
    is(AluSrcB.IMM_B) {
      aluOperandB := instructionSplitter.io.imm_sb_type.asSInt.resize(XLEN).asBits
    }
    is(AluSrcB.IMM_J) {
      aluOperandB := instructionSplitter.io.imm_j_type.asSInt.resize(XLEN).asBits
    }
  }


  // === Exceptions ===

  // Misaligned fetch.
  // Instruction address must be aligned to a multiple of 4.
  val exception_misaligned_fetch = pc(0 to 1) =/= 0

  any_exception := exception_invalid_instruction | exception_misaligned_load_store | exception_misaligned_fetch

  when(exception_invalid_instruction) {
    //    exception_cause := instructionDecoderexception_cause
    // TODO
  } elsewhen (exception_misaligned_fetch) {
    exception_cause := U(Causes.misaligned_fetch)
  } elsewhen (exception_misaligned_load_store) {
    exception_cause := U(Causes.misaligned_load)
    // TODO make distinction between load/store
    //    when(mem_load_operation_pending) {
    //      exception_cause := U(Causes.misaligned_load)
    //    } otherwise {
    //      exception_cause := U(Causes.misaligned_store)
    //    }
  }

  // === Instruction fetch unit ===

  def doFetchNewInstruction(): Unit = {
    fetchNewInstruction := True
    // Declare to be ready to take an instruction immediately. Don't wait for request to be acknowledged.
    acceptNewInstruction := True
  }

  when(fetchNewInstruction) {
    io.i_req_valid_o := True
    when(io.i_req_ready_i) {
      fetchNewInstruction := False
      //      acceptNewInstruction := True
    }
  }

  when(acceptNewInstruction) {
    io.i_rsp_ready_o := True
    when(io.i_rsp_valid_i) {
      acceptNewInstruction := False
      instructionReg := io.i_rsp_data_i
      instructionPresent := True
    }
  }
  //  // Test for debugging iverilog delta timing issue.
  //  val testReg1 = RegInit(False)
  //  val testReg2 = RegInit(False)
  //  val testSignal = False
  //  testReg1.keep()
  //  testReg2.keep()
  //  when(testSignal) {
  //    testReg2 := True
  //  }
  //  when(io.wakeup_i) {
  //    testSignal := True
  //    testReg1 := True
  //  }
  //  assert(testReg1 === testReg2, "testReg1 and testReg2 should have the same value!")

  // === Control FSM ===

  val fsmMain = new StateMachine {

    val stateDormant: State = new State with EntryPoint {
      // Dormant state, waiting for wakeup_i to be high.
      whenIsActive {
        when(io.wakeup_i) {
          // Wakeup!
          when(!io.wakeup_resume_i) {
            // Load external boot address.
            pc_next := io.wakeup_addr_i
            updatePC()
          } otherwise {
            // Either start from the default boot address (after a RESET)
            // or continue executing code after the WFI instruction that
            // triggered the dormant state.
          }
          // Trigger loading an instruction.
          doFetchNewInstruction()
          goto(stateDecodeExecute)
        }
      }


      val stateDecodeExecute: State = new State {

        whenIsActive {

          // Wait for instruction to be valid.
          when(instructionPresent) {

            when(any_exception) {
              // TODO: reachable?
              // TODO Commit instruction on error?
              instructionPresent := False
              goto(stateError)
            } elsewhen (instructionCommitStrobe) {

              // Commit instruction.
              updatePC()
              instructionPresent := False

              when(waitForInterrupt) {
                goto(stateDormant)
              } otherwise {
                doFetchNewInstruction()
              }
            }
          }
        }
      }

      val stateError = new State {
        whenIsActive {
          // TODO: how to reboot?
          io.exc_valid_o := True
          io.exc_cause_o := exception_cause
        }
      }
    }

  }
}


/**
 * Top-level of Ottocore implementation.
 *
 * See: https://codeberg.org/ottocore/ottocore-framework
 */
class ottocore extends Component {

  val XLEN = 32 bits // register size

  val io = new Bundle {
    // status signals
    val clk_i = in Bool
    val rst_ni = in Bool

    // launch core
    val wakeup_i = in Bool
    val wakeup_resume_i = in Bool
    val wakeup_addr_i = in UInt (XLEN)

    // exception handling
    val exc_valid_o = out Bool
    val exc_cause_o = out UInt (XLEN)

    // instruction bus
    val i_req_addr_o = out UInt (XLEN)
    val i_req_ready_i = in Bool
    val i_req_valid_o = out Bool // address is valid

    val i_rsp_data_i = in Bits (XLEN)
    val i_rsp_valid_i = in Bool // data is ready
    val i_rsp_ready_o = out Bool

    // data bus
    val d_req_addr_o = out UInt (XLEN)
    val d_req_wdata_o = out Bits (XLEN)
    val d_req_wstrb_o = out Bits (XLEN.value / 8 bits)
    val d_req_ready_i = in Bool
    val d_req_valid_o = out Bool

    val d_rsp_rdata_i = in Bits (XLEN)
    val d_rsp_valid_i = in Bool
    val d_rsp_ready_o = out Bool
  }

  // Don't append `io_` to the verilog signals.
  noIoPrefix()

  // Create a clock domain driven by `clk_i` and `rst_ni`.

  val clkDomain = new ClockDomain(
    clock = io.clk_i,
    reset = io.rst_ni,
    config = DefaultClockDomainConfig()
  )
  new ClockingArea(clkDomain) {
    val core = DefaultOttocore()
    core.io <> io
  }
}

object DefaultClockDomainConfig {
  def apply(): ClockDomainConfig = ClockDomainConfig(
    resetKind = SYNC,
    resetActiveLevel = LOW,
    clockEdge = RISING
  )
}

object AttocoreSpinalConfig extends SpinalConfig(
  // Require synchronous reset and rising edge clock.
  defaultConfigForClockDomains = DefaultClockDomainConfig()
)

/**
 * Define a core with a default set of plugins.
 */
object DefaultOttocore {


  def defaultPlugins(): List[Plugin] = {

    import Instructions._
    List(
      new RegisterFilePlugin(numRegisters = 16, numFormalRegisters = 3, resetToZero = true),
      new InstructionDecoderPlugin,
      new IntAluPlugin,
      new ShiftPlugin,
//      new TwoCycleBranchPlugin,
      new SingleCycleBranchPlugin(),
      new JumpPlugin,
      new LoadStorePlugin,
      new UTypeInstructionPlugin,
      new WFIPlugin,
      new DummyNOPs(
        // Implement this instructions as NOPs.
        List(
          ECALL, EBREAK, FENCE, FENCE_I
        )
      )
    )
  }

  def apply(): Attocore = {
    new Attocore(defaultPlugins())
  }
}

/**
 * Generate the ottocore verilog.
 */
object OttocoreVerilog {
  def main(args: Array[String]) {
    AttocoreSpinalConfig.generateVerilog(
      new ottocore()
    )
  }
}

object AttocoreFormalSystemVerilog {
  def main(args: Array[String]) {
    AttocoreSpinalConfig.includeFormal.generateSystemVerilog(DefaultOttocore())
  }
}