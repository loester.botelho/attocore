package ottocore

import spinal.core._
import spinal.lib._

object ComparatorCtrl extends SpinalEnum {
  val Signed, Unsigned = newElement()
}

case class ComparatorCmd(XLEN: BitCount = 32 bits) extends Bundle {
  val operand_a = Bits(XLEN)
  val operand_b = Bits(XLEN)
  val control = ComparatorCtrl()
}

case class ComparatorRsp(XLEN: BitCount = 32 bits) extends Bundle {
  val difference = Bits(XLEN) // a - b
  val aEqualsB = Bool()
  val aLessThanB = Bool()
  val control = ComparatorCtrl()
}

class ComparatorUnit extends Component {

  val XLEN = 32 bits // register size

  val io = new Bundle {
    val cmd = slave Stream ComparatorCmd(XLEN)
    val rsp = master Stream ComparatorRsp(XLEN)
  }

  // Cast operands to UInt.
  val a = io.cmd.operand_a.asUInt
  val b = io.cmd.operand_b.asUInt

  val sub = (a - b).asBits

  val result = Bits(XLEN)

  val unsignedLess = False

  switch(io.cmd.control) {
    import ComparatorCtrl._
    is(Unsigned) {
      unsignedLess := False
    }
    is(Signed) {
      unsignedLess := True
    }
  }

  // Compute comparison result.
  val relevant_msb = Mux(unsignedLess, b.msb, a.msb)
  val aLessThanB = Mux(a.msb === b.msb, sub.msb, relevant_msb)

  // TODO: 2 outputs for signed and unsigned comparision!

  val aEqualsB = !sub.orR

  // Connect to the output.
  val resultBundle = ComparatorRsp()
  resultBundle.difference := sub
  resultBundle.aEqualsB := aEqualsB
  resultBundle.aLessThanB := aLessThanB
  resultBundle.control := io.cmd.control
  io.rsp << io.cmd.translateWith(resultBundle)

}
