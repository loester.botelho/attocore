package ottocore

import org.scalatest.FlatSpec
import ottocore.plugins.RegisterFile
import spinal.core.sim._

import scala.util.Random

class RegisterFileModel {
  val NUM_REGISTERS = 16
  val registers = (0 until NUM_REGISTERS).map(_ => BigInt(0)).toArray

  def readRegister(index: Int): BigInt = {
    if (index == 0) {
      BigInt(0)
    } else {
      registers(index)
    }
  }

  def writeRegister(index: Int, value: BigInt): Unit = {
    if (index != 0) {
      registers(index) = value
    }
  }
}

class RegisterFileSpec extends FlatSpec {

  val compiled = SimConfig.compile(new RegisterFile)

  "A register file" should " behave the same as the model under random inputs." in {

    compiled.doSim { dut =>

      Random.setSeed(42)

      dut.io.w_valid #= false
      dut.io.r_addr1 #= 0
      dut.io.r_addr2 #= 0
      dut.io.w_addr #= 0
      dut.io.w_data #= 0

      dut.clockDomain.assertReset()
      sleep(10)
      dut.clockDomain.deassertReset()
      sleep(10)
      dut.clockDomain.forkStimulus(period = 10)

      dut.clockDomain.waitActiveEdge()

      // Assert that outputs are zero.
      assertResult(0)(dut.io.r_data1.toBigInt)
      assertResult(0)(dut.io.r_data2.toBigInt)

      val model = new RegisterFileModel

      for (idx <- 0 to 1<<12) {

        val w_addr = Random.nextInt(model.NUM_REGISTERS)
        val w_valid = Random.nextBoolean()
        val w_data = Random.nextInt(1 << 10)
        dut.io.w_addr #= w_addr
        dut.io.w_valid #= w_valid
        dut.io.w_data #= w_data

        val r_addr1 = Random.nextInt(model.NUM_REGISTERS)
        val r_addr2 = Random.nextInt(model.NUM_REGISTERS)
        dut.io.r_addr1 #= r_addr1
        dut.io.r_addr2 #= r_addr2

        dut.clockDomain.waitActiveEdge()

        assertResult(model.readRegister(r_addr1))(dut.io.r_data1.toBigInt)
        assertResult(model.readRegister(r_addr2))(dut.io.r_data2.toBigInt)

        println(s"reg($r_addr1) = ${dut.io.r_data1.toBigInt}")
        println(s"reg($r_addr2) = ${dut.io.r_data2.toBigInt}")

        if (w_valid) {
          println(s"reg($w_addr) <- $w_data")
          model.writeRegister(w_addr, w_data)
        }

      }
    }
  }

}
