package ottocore

import org.scalatest.FlatSpec
import spinal.core.MaskedLiteral
import spinal.core.sim._
import spinal.core._

import scala.collection.mutable
import scala.util.Random

/**
 * Simple assembler for RISCV instructions.
 */
object Op {

  def create_bits(range: Range, value_signed: Long): BigInt = {
    val value = value_signed & ((1l << (range.length)) - 1)
    assert(value < (1 << range.length), s"Value ($value) does not fit in the number of bits (${range.length}).")
    (BigInt(value) << range.start)
  }

  def R(opcode: MaskedLiteral, rd: Int, rs1: Int, rs2: Int): BigInt = {
    val value = opcode.value
    val fixed = opcode.careAbout

    val rd_bits = create_bits(7 to 11, rd)
    val rs1_bits = create_bits(15 to 19, rs1)
    val rs2_bits = create_bits(20 to 24, rs2)

    val variable = rd_bits | rs1_bits | rs2_bits

    assert((variable & fixed) == 0, "Tried to write into fixed bits.")

    value | variable
  }

  def I(opcode: MaskedLiteral, rd: Int, rs1: Int, imm: Int): BigInt = {
    val value = opcode.value
    val fixed = opcode.careAbout

    val rd_bits = create_bits(7 to 11, rd)
    val rs1_bits = create_bits(15 to 19, rs1)
    val imm_bits = create_bits(20 to 31, imm)

    val variable = rd_bits | rs1_bits | imm_bits

    assert((variable & fixed) == 0, "Tried to write into fixed bits.")

    value | variable
  }

  def S(opcode: MaskedLiteral, imm: Int, rs1: Int, rs2: Int): BigInt = {
    val value = opcode.value
    val fixed = opcode.careAbout

    val imm_bits = create_bits(25 to 31, imm >> 5) | create_bits(7 to 11, imm & 0x1f)
    val rs1_bits = create_bits(15 to 19, rs1)
    val rs2_bits = create_bits(20 to 24, rs2)

    val variable = imm_bits | rs1_bits | rs2_bits

    assert((variable & fixed) == 0, "Tried to write into fixed bits.")

    value | variable
  }

  def B(opcode: MaskedLiteral, imm: BigInt, rs1: Int, rs2: Int): BigInt = {
    assert(!imm.testBit(0), "Branch must be aligned to multiples of 2 bytes.")

    // Reorder bits.
    // Put bit 11 at lowest position, shift the rest accordingly.
    val bitorder = List(12, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 11)
    val reordered = bitorder.map(imm.testBit)
      .map(if (_) 1 else 0)
      .fold(0)((a, b) => (a << 1) | b)

    S(opcode, reordered, rs1, rs2)
  }

  def U(opcode: MaskedLiteral, rd: Int, imm: Int): BigInt = {
    val value = opcode.value
    val fixed = opcode.careAbout

    val rd_bits = create_bits(7 to 11, rd)
    val imm_bits = create_bits(12 to 31, imm)

    val variable = rd_bits | imm_bits

    assert((variable & fixed) == 0, "Tried to write into fixed bits.")

    value | variable
  }

  def J(opcode: MaskedLiteral, rd: Int, imm_long: Long): BigInt = {

    val imm = BigInt(imm_long & 0x1fffff)

    assert(!imm.testBit(0), "Jump must be aligned to multiples of 2 bytes.")

    val value = opcode.value
    val fixed = opcode.careAbout

    val rd_bits = create_bits(7 to 11, rd)

    // Reoreder bits of immediate value.
    val bitorder = List(20, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 11, 19, 18, 17, 16, 15, 14, 13, 12)
    val reordered = bitorder.map(imm.testBit)
      .map(if (_) 1 else 0)
      .fold(0)((a, b) => (a << 1) | b)

    val imm_bits = create_bits(12 to 31, reordered)

    val variable = rd_bits | imm_bits

    assert((variable & fixed) == 0, "Tried to write into fixed bits.")

    value | variable
  }
}

class AttocoreSpec extends FlatSpec {

  val compiled = SimConfig.withWave.compile(DefaultOttocore())

  def bootCore(dut: Attocore, boot_addr: BigInt = 0x80000000l, wakeup_resume: Boolean = false): Unit = {
    dut.io.wakeup_addr_i #= boot_addr
    dut.io.wakeup_resume_i #= wakeup_resume
    dut.io.wakeup_i #= false

    dut.io.i_req_ready_i #= false
    dut.io.i_rsp_data_i #= 0 // Invalid instruction.
    dut.io.i_rsp_valid_i #= false

    dut.clockDomain.assertReset()
    sleep(10)
    dut.clockDomain.deassertReset()
    sleep(10)
    dut.clockDomain.forkStimulus(period = 10)

    dut.clockDomain.waitActiveEdge()

    dut.io.wakeup_i #= true
    dut.clockDomain.waitActiveEdge()
    dut.io.wakeup_i #= false
  }

  def assertTerminates(dut: Attocore, max_cycles: Int = 1000): Unit = {
    fork {
      // Abort if core does not boot within some finite time.
      dut.clockDomain.waitActiveEdge(max_cycles)
      assert(false, s"Test did not complete within $max_cycles cycles.")
    }
  }

  /**
   * Watch for an exception thrown by the core.
   *
   * @param dut
   */
  def watchErrors(dut: Attocore): Unit = {
    fork {
      // Watch error flag.
      dut.clockDomain.waitActiveEdgeWhere(dut.io.exc_valid_o.toBoolean)
      assert(false, s"Core raised an error! (cause = ${dut.io.exc_cause_o.toBigInt})")
    }
  }

  def watchDataMemoryResponses(dut: Attocore): Unit = {
    fork {
      while (true) {
        dut.clockDomain.waitActiveEdgeWhere(dut.io.d_rsp_valid_i.toBoolean & dut.io.d_rsp_ready_o.toBoolean)
        val data = dut.io.d_rsp_rdata_i.toBigInt
        println(s"Data memory response: ${data}")
      }
    }
  }

  def watchDataMemoryRequests(dut: Attocore): Unit = {
    fork {
      while (true) {
        dut.clockDomain.waitActiveEdgeWhere(dut.io.d_req_valid_o.toBoolean & dut.io.d_req_ready_i.toBoolean)
        val data = dut.io.d_req_wdata_o.toBigInt
        val addr = dut.io.d_req_addr_o.toBigInt
        val strb = dut.io.d_req_wstrb_o.toBigInt

        println(s"Data memory request: ADDR=${addr}, WSTRB=${strb}, DATA=${data}")
      }
    }
  }

  /**
   * Wait for an instruction request, then apply the given instruction code and wait until
   * instruction is acknowledged by the `i_rsp_ready_o` flag.
   *
   * @param dut
   * @param instruction
   */
  def executeInstruction(dut: Attocore, instruction: BigInt): Unit = {

    assert(!dut.io.i_req_ready_i.toBoolean, "i_req_ready_i is already HIGH. Cannot guarantee correctness.")
    dut.io.i_req_ready_i #= true

    println("waiting for instruction request")

    dut.clockDomain.waitActiveEdgeWhere(dut.io.i_req_valid_o.toBoolean)

    dut.io.i_req_ready_i #= false

    val reqAddr = dut.io.i_req_addr_o.toBigInt
    println(s"Instruction request addr: $reqAddr")
    println("issue instruction")
    // Apply valid instruction.
    dut.io.i_rsp_valid_i #= true
    // Issue S-type instruction.
    dut.io.i_rsp_data_i #= instruction
    dut.clockDomain.waitActiveEdgeWhere(dut.io.i_rsp_ready_o.toBoolean)
    println("instruction completed")
    dut.io.i_rsp_valid_i #= false
  }


  /**
   * Helper function for reading or writing a register via the memory bus.
   *
   * @param dut
   * @param reg
   * @param value
   * @return
   */
  def loadStoreReg(dut: Attocore, reg: Int, value: Option[BigInt]): BigInt = {

    dut.io.d_rsp_valid_i #= false
    dut.io.d_req_ready_i #= true
    dut.io.d_rsp_rdata_i #= value.getOrElse(BigInt(0))

    val op = value match {
      case Some(v) => Op.I(Instructions.LW, reg, 0, 0)
      case None => Op.S(Instructions.SW, 0x00, 0, reg)
    }
    fork {
      executeInstruction(dut, op)
    }

    // Wait for store request.
    dut.clockDomain.waitActiveEdgeWhere(dut.io.d_req_valid_o.toBoolean)
    val retValue = dut.io.d_req_wdata_o.toBigInt

    dut.io.d_req_ready_i #= false


    // Response handshake.
    dut.io.d_rsp_valid_i #= true
    dut.clockDomain.waitActiveEdgeWhere(dut.io.d_rsp_ready_o.toBoolean)

    dut.io.d_rsp_valid_i #= false
    dut.io.d_rsp_rdata_i #= 0

    retValue
  }

  /**
   * Helper function for reading a register over the memory bus.
   *
   * @param dut
   * @param reg
   * @return
   */
  def readRegister(dut: Attocore, reg: Int): BigInt = {
    assert(reg <= 15)
    loadStoreReg(dut, reg, None)
  }

  /**
   * Helper function for writing a register over the memory bus.
   *
   * @param dut
   * @param reg
   * @param data
   */
  def writeRegister(dut: Attocore, reg: Int, data: BigInt): Unit = {
    assert(reg <= 15)
    loadStoreReg(dut, reg, Some(data))
    ()
  }

  /**
   * Drive the memory I/O ports.
   * Calls `load(addr)` on a load request.
   * Calls `store(addr, wdata)` on a store request.
   *
   * @param load  Callback for load requests.
   * @param store Callback for store requests.
   */
  def memControl(
                  clockDomain: ClockDomain,
                  req_ready_i: Bool,
                  req_valid_o: Bool,
                  req_wstrb_o: Option[Bits],
                  req_wdata_o: Option[Bits],
                  req_addr_o: UInt,
                  rsp_ready_o: Bool,
                  rsp_valid_i: Bool,
                  rsp_rdata_i: Bits,
                  load: BigInt => BigInt,
                  store: (BigInt, BigInt) => Unit
                ): Unit = {

    fork {
      while (true) {
        // Wait for request.
        req_ready_i #= true
        clockDomain.waitActiveEdgeWhere(req_valid_o.toBoolean && req_ready_i.toBoolean)
        req_ready_i #= false
        val wstrb = req_wstrb_o.map(_.toBigInt).getOrElse(BigInt(0))
        val wdata = req_wdata_o.map(_.toBigInt).getOrElse(BigInt(0))
        val addr = req_addr_o.toBigInt

        if (wstrb == 0) {
          // Load request.

          // Issue response.
          val rdata = load(addr)
          rsp_rdata_i #= rdata
          rsp_valid_i #= true
          clockDomain.waitActiveEdgeWhere(rsp_ready_o.toBoolean)
          rsp_valid_i #= false
          rsp_rdata_i #= 0
        } else {
          // Store request.

          // Generate bitmask from wstrb.
          val bitmask = (0 to 3).map(i => {
            val bit = if (wstrb.testBit(3)) {
              1l
            } else {
              0l
            }
            val byte = (bit << 8) - 1
            byte << (8 * i)
          }).reduce(_ | _)

          val oldData = load(addr)
          val newData = (oldData & ~bitmask) | (wdata & bitmask)
          store(addr, newData)

          // Acknowledge store request.
          rsp_valid_i #= true
          clockDomain.waitActiveEdgeWhere(rsp_ready_o.toBoolean)
          rsp_valid_i #= false
        }
      }
    }

  }

  "An ottocore after reset" should "boot from the default address when `wakeup_resume_i` is HIGH." in {

    compiled.doSim { dut =>
      Random.setSeed(42)

      bootCore(dut, 0, wakeup_resume = true)

      assertTerminates(dut)

      dut.clockDomain.waitActiveEdgeWhere(dut.io.i_req_valid_o.toBoolean)
      assertResult(0x80000000l)(dut.io.i_req_addr_o.toBigInt)
    }
  }


  it should "boot from the supplied address when `wakeup_resume_i` is LOW." in {

    compiled.doSim { dut =>
      Random.setSeed(42)

      val boot_addr = 0x12345678l
      bootCore(dut, boot_addr, wakeup_resume = false)

      assertTerminates(dut)

      dut.clockDomain.waitActiveEdgeWhere(dut.io.i_req_valid_o.toBoolean)
      assertResult(boot_addr)(dut.io.i_req_addr_o.toBigInt)
    }
  }


  "The core" should "raise an error if it loads an invalid instruction." in {

    compiled.doSim { dut =>
      Random.setSeed(42)

      bootCore(dut, boot_addr = 0)

      assertTerminates(dut)

      // Let the core load an invalid instruction.
      fork {
        dut.clockDomain.waitActiveEdgeWhere(dut.io.i_req_valid_o.toBoolean)
        dut.io.i_rsp_valid_i #= true
        dut.io.i_req_ready_i #= true
        dut.clockDomain.waitActiveEdgeWhere(dut.io.i_rsp_ready_o.toBoolean)
        dut.io.i_rsp_valid_i #= false
      }

      dut.clockDomain.waitActiveEdgeWhere(dut.io.exc_valid_o.toBoolean)
      assertResult(true, "No error reported by core.")(dut.io.exc_valid_o.toBoolean)
      assertResult(Causes.illegal_instruction, "Wrong error cause reported by core.")(dut.io.exc_cause_o.toBigInt)
    }
  }

  it should "execute one R instruction after the other" in {

    compiled.doSim { dut =>
      Random.setSeed(42)

      bootCore(dut, boot_addr = 0)

      assertTerminates(dut)

      watchErrors(dut)

      for (i <- 0 until 16) {
        // Let the core load an R instruction.
        executeInstruction(dut, Op.R(Instructions.ADD, Random.nextInt(16), Random.nextInt(16), Random.nextInt(16)))
      }
    }
  }

  it should "properly write to memory" in {

    compiled.doSim { dut =>
      Random.setSeed(42)

      bootCore(dut, boot_addr = 0)

      assertTerminates(dut)

      watchErrors(dut)

      dut.io.i_req_ready_i #= true

      val numStoreInstructions = 16

      // Load immediate value into r1: r1 <- 42
      executeInstruction(dut, Op.I(Instructions.ADDI, 1, 0, 42))

      fork {
        for (i <- 1 to numStoreInstructions) {
          // Let the core execute a store instruction.
          executeInstruction(dut, Op.S(Instructions.SW, i * 4, 0, 1))
        }
      }

      for (i <- 1 to numStoreInstructions) {
        dut.io.d_rsp_valid_i #= false
        dut.io.d_req_ready_i #= true
        // Wait for store request.
        dut.clockDomain.waitActiveEdgeWhere(dut.io.d_req_valid_o.toBoolean)
        println(s"MEM Store request: WSTRB = ${dut.io.d_req_wstrb_o.toBigInt}," +
          s" WADDR = ${dut.io.d_req_addr_o.toBigInt}," +
          s" WDATA = ${dut.io.d_req_wdata_o.toBigInt}")

        assertResult(i * 4, "Store address.")(dut.io.d_req_addr_o.toBigInt)
        assertResult(0xf, "Byte strobe.")(dut.io.d_req_wstrb_o.toBigInt)
        assertResult(42, "Unexpected WDATA.")(dut.io.d_req_wdata_o.toBigInt)

        // Complete rsp handshake
        dut.io.d_rsp_valid_i #= true
        dut.clockDomain.waitActiveEdgeWhere(dut.io.d_rsp_ready_o.toBoolean)
        dut.io.d_rsp_valid_i #= false
      }


    }
  }


  it should "properly store words, half words and bytes to memory" in {

    compiled.doSim { dut =>
      Random.setSeed(42)

      bootCore(dut, boot_addr = 0)

      assertTerminates(dut)

      watchErrors(dut)

      dut.io.i_req_ready_i #= true

      val data = 0x12345678l

      // Expected responses for 3x load & store operations.
      val expected_data = List(
        0, data,
        data & 0xffffffffffl,
        (data << 16) & 0xffff0000l,
        data & 0xffffffffl,
        (data << 8) & 0xffffff00l,
        (data << 16) & 0xffff0000l,
        (data << 24) & 0xff000000l
      )
      val expected_wstrobe = List(
        0, 0xf,
        0x3, // LH, SH
        0x3 << 2, // LH, SH
        1 << 0, // LB, SB
        1 << 1, // LB, SB
        1 << 2, // LB, SB
        1 << 3 // LB, SB
      )
      val expected_addr = List(
        4, 4, // Word
        8, // Half-word
        10, // Half-word
        12, // Byte
        13, // Byte
        14, // Byte
        15 // Byte
      )

      fork {
        for (i <- 0 until 1000) {
          dut.io.d_req_ready_i #= true
          dut.io.d_rsp_rdata_i #= data
          dut.io.d_rsp_valid_i #= true
          // Wait for store request.
          dut.clockDomain.waitActiveEdgeWhere(dut.io.d_req_valid_o.toBoolean)

          println(
            s"MEM request: WSTRB = ${dut.io.d_req_wstrb_o.toBigInt}, " +
              s"WADDR = ${dut.io.d_req_addr_o.toBigInt}, " +
              s"WDATA = ${dut.io.d_req_wdata_o.toBigInt}"
          )

          assertResult(expected_addr(i), "Load address.")(dut.io.d_req_addr_o.toBigInt)
          assertResult(expected_wstrobe(i), "Byte strobe is wrong.")(dut.io.d_req_wstrb_o.toBigInt)
          assertResult(expected_data(i), "Wrong data.")(dut.io.d_req_wdata_o.toBigInt)

        }
      }

      // Load word from address 0 into register 2.
      executeInstruction(dut, Op.I(Instructions.LW, 2, 0, 4))
      executeInstruction(dut, Op.S(Instructions.SW, 4, 0, 2))

      // Load half word from address 8 into register 1.
      executeInstruction(dut, Op.S(Instructions.SH, 8, 0, 2))

      // Load half word from address 10 into register 1.
      executeInstruction(dut, Op.S(Instructions.SH, 10, 0, 2))

      // Load byte from address 12 into register 1.
      executeInstruction(dut, Op.S(Instructions.SB, 12, 0, 2))

      // Load byte from address 13 into register 1.
      executeInstruction(dut, Op.S(Instructions.SB, 13, 0, 2))

      // Load byte from address 14 into register 1.
      executeInstruction(dut, Op.S(Instructions.SB, 14, 0, 2))

      // Load byte from address 15 into register 1.
      executeInstruction(dut, Op.S(Instructions.SB, 15, 0, 2))
    }
  }


  it should "properly load words, half words and bytes from memory" in {

    compiled.doSim { dut =>
      Random.setSeed(42)

      bootCore(dut, boot_addr = 0)

      assertTerminates(dut)

      watchErrors(dut)
      watchDataMemoryRequests(dut)
      watchDataMemoryResponses(dut)


      val data = 0xaabbccddl

      /**
       * Load data into a register using one of the load instructions
       * read it again (by SW instruction) and compare against expected value.
       *
       * @param instruction
       * @param address
       * @param expected_data
       */
      def testLoad(instruction: MaskedLiteral, address: Int, expected_data: Long) {
        val destReg = 1
        dut.io.d_req_ready_i #= true
        dut.io.d_rsp_rdata_i #= data
        dut.io.d_rsp_valid_i #= true
        fork {
          executeInstruction(dut, Op.I(instruction, destReg, 0, address))
        }

        // Wait for store request.
        dut.clockDomain.waitActiveEdgeWhere(dut.io.d_req_valid_o.toBoolean)
        val retValue = dut.io.d_req_wdata_o.toBigInt

        dut.io.d_req_ready_i #= false


        // Response handshake.
        dut.io.d_rsp_valid_i #= true
        dut.clockDomain.waitActiveEdgeWhere(dut.io.d_rsp_ready_o.toBoolean)
        dut.io.d_rsp_valid_i #= false
        dut.io.d_rsp_rdata_i #= 0

        assertResult(expected_data)(readRegister(dut, destReg))
      }

      testLoad(Instructions.LW, 0, 0xaabbccddl)
      testLoad(Instructions.LW, 0, 0xaabbccddl)

      testLoad(Instructions.LH, 0, 0xffffccddl)
      testLoad(Instructions.LH, 2, 0xffffaabbl)
      testLoad(Instructions.LHU, 0, 0x0000ccddl)
      testLoad(Instructions.LHU, 2, 0x0000aabbl)

      testLoad(Instructions.LB, 0, 0xffffffddl)
      testLoad(Instructions.LB, 1, 0xffffffccl)
      testLoad(Instructions.LB, 2, 0xffffffbbl)
      testLoad(Instructions.LB, 3, 0xffffffaal)

      testLoad(Instructions.LBU, 0, 0x000000ddl)
      testLoad(Instructions.LBU, 1, 0x000000ccl)
      testLoad(Instructions.LBU, 2, 0x000000bbl)
      testLoad(Instructions.LBU, 3, 0x000000aal)
    }
  }

  it should "properly perform sign extension for LOAD operations." in {

    compiled.doSim { dut =>
      Random.setSeed(42)

      bootCore(dut, boot_addr = 0)
      assertTerminates(dut)
      watchErrors(dut)

      writeRegister(dut, 1, 1)
      assertResult(1)(readRegister(dut, 1))

      // TODO: write checks for sign extension.

    }
  }

  it should "properly execute ADDI, SUB, SLT" in {

    compiled.doSim { dut =>
      Random.setSeed(42)

      bootCore(dut, boot_addr = 0)
      assertTerminates(dut)
      watchErrors(dut)

      val reg = 3
      executeInstruction(dut, Op.I(Instructions.ADDI, reg, 0, 7))
      assertResult(7)(readRegister(dut, reg))

      executeInstruction(dut, Op.I(Instructions.ADDI, reg, reg, 1))
      assertResult(8)(readRegister(dut, reg))

      // Check sign extension of immediate.
      executeInstruction(dut, Op.I(Instructions.ADDI, 1, 0, -1))
      assertResult(0xffffffffl)(readRegister(dut, 1))

      // r2 = r1 - r1
      executeInstruction(dut, Op.R(Instructions.SUB, 2, 1, 1))
      assertResult(0)(readRegister(dut, 2))

      // r1 = 1
      executeInstruction(dut, Op.I(Instructions.ADDI, 1, 0, 1))
      // r2 = r0 - r1
      executeInstruction(dut, Op.R(Instructions.SUB, 2, 0, 1))
      assertResult(0xffffffffl)(readRegister(dut, 2))

      val one = 1
      val two = 2
      val minusOne = 3
      val minusTwo = 4

      writeRegister(dut, one, 1)
      writeRegister(dut, two, 2)
      writeRegister(dut, minusOne, 0xffffffffl)
      writeRegister(dut, minusTwo, 0xfffffffel)

      assertResult(0xffffffffl)(readRegister(dut, minusOne))
      assertResult(0xfffffffel)(readRegister(dut, minusTwo))

      val slt = 5
      // one < two
      executeInstruction(dut, Op.R(Instructions.SLT, slt, one, two))
      assertResult(1)(readRegister(dut, slt))

      // two < one
      executeInstruction(dut, Op.R(Instructions.SLT, slt, two, one))
      assertResult(0)(readRegister(dut, slt))

      // -1 < -2
      executeInstruction(dut, Op.R(Instructions.SLT, slt, minusOne, minusTwo))
      assertResult(0)(readRegister(dut, slt))

      // -2 < -1
      executeInstruction(dut, Op.R(Instructions.SLT, slt, minusTwo, minusOne))
      assertResult(1)(readRegister(dut, slt))

      // 1 < -1
      executeInstruction(dut, Op.R(Instructions.SLT, slt, one, minusOne))
      assertResult(0)(readRegister(dut, slt))

      // -1 < 1
      executeInstruction(dut, Op.R(Instructions.SLT, slt, minusOne, one))
      assertResult(1)(readRegister(dut, slt))

    }
  }


  it should "properly execute ADDI, XORI, ORI, ANDI" in {

    compiled.doSim { dut =>
      Random.setSeed(42)

      bootCore(dut, boot_addr = 0)
      assertTerminates(dut)
      watchErrors(dut)

      val aVal = 0xa5
      val bVal = 0xf5

      val a = 1
      val b = 2
      val resultReg = 3

      writeRegister(dut, a, aVal)
      writeRegister(dut, b, bVal)

      assertResult(aVal)(readRegister(dut, a))
      assertResult(bVal)(readRegister(dut, b))

      // ADDI
      executeInstruction(dut, Op.I(Instructions.ADDI, resultReg, a, bVal))
      assertResult(aVal + bVal)(readRegister(dut, resultReg))

      // XORI
      executeInstruction(dut, Op.I(Instructions.XORI, resultReg, a, bVal))
      assertResult(aVal ^ bVal)(readRegister(dut, resultReg))

      // ORI
      executeInstruction(dut, Op.I(Instructions.ORI, resultReg, a, bVal))
      assertResult(aVal | bVal)(readRegister(dut, resultReg))

      // ANDI
      executeInstruction(dut, Op.I(Instructions.ANDI, resultReg, a, bVal))
      assertResult(aVal & bVal)(readRegister(dut, resultReg))
    }
  }


  it should "properly execute SLL, SRL, SRA, SLLI, SRLI, SRAI" in {

    compiled.doSim { dut =>
      assertResult(32)(dut.XLEN.value)

      Random.setSeed(42)

      bootCore(dut, boot_addr = 0)
      assertTerminates(dut)
      watchErrors(dut)


      // data values and shift amounts.
      val valueShiftAmounts = List(
        (0xaaaaaaaal, 0),
        (0xaaaaaaaal, 1),
        (0xaaaaaaaal, 2),
        (0xaaaaaaaal, 15),
        (0xaaaaaaaal, 16),
        (0xaaaaaaaal, 31),
        (0x55555555l, 1),
        (0x55555555l, 2),
        (0x55555555l, 15),
        (0x55555555l, 16),
        (0x55555555l, 31)
      )

      val xlen = dut.XLEN.value

      for ((value, shiftAmount) <- valueShiftAmounts) {

        // Compute expected values.
        val mask = (BigInt(1) << xlen) - 1
        val valueShiftLeft = (BigInt(value) << shiftAmount) & mask
        val valueShiftRightL = (value >> shiftAmount) & mask
        val valueShiftRightA = (value.toInt >> shiftAmount).toLong & mask

        val regValue = 1
        val regShamt = 2
        val regResult = 3

        writeRegister(dut, regValue, value)
        writeRegister(dut, regShamt, shiftAmount)

        // SLL
        writeRegister(dut, regResult, 0)
        executeInstruction(dut, Op.R(Instructions.SLL, regResult, regValue, regShamt))
        assertResult(valueShiftLeft)(readRegister(dut, regResult))

        // SRL
        writeRegister(dut, regResult, 0)
        executeInstruction(dut, Op.R(Instructions.SRL, regResult, regValue, regShamt))
        assertResult(valueShiftRightL)(readRegister(dut, regResult))

        // SRA
        writeRegister(dut, regResult, 0)
        executeInstruction(dut, Op.R(Instructions.SRA, regResult, regValue, regShamt))
        assertResult(valueShiftRightA)(readRegister(dut, regResult))

        // SLLI
        writeRegister(dut, regResult, 0)
        executeInstruction(dut, Op.I(Instructions.SLLI, regResult, regValue, shiftAmount))
        assertResult(valueShiftLeft)(readRegister(dut, regResult))

        // SRLI
        writeRegister(dut, regResult, 0)
        executeInstruction(dut, Op.I(Instructions.SRLI, regResult, regValue, shiftAmount))
        assertResult(valueShiftRightL)(readRegister(dut, regResult))

        // SRLI
        writeRegister(dut, regResult, 0)
        executeInstruction(dut, Op.I(Instructions.SRAI, regResult, regValue, shiftAmount))
        assertResult(valueShiftRightA)(readRegister(dut, regResult))
      }
    }
  }


  it should "properly execute LUI" in {

    compiled.doSim { dut =>
      Random.setSeed(42)

      bootCore(dut, boot_addr = 0)
      assertTerminates(dut)
      watchErrors(dut)

      val resultReg = 3

      // Test LUI
      val values = List(1, 0, 1, 2, 3, 0xfffff, 0x80000)
      for (v <- values) {
        executeInstruction(dut, Op.U(Instructions.LUI, resultReg, v))
        assertResult(v.toLong << 12)(readRegister(dut, resultReg))
      }

    }
  }


  it should "properly execute AUIPC" in {

    compiled.doSim { dut =>
      Random.setSeed(42)
      val boot_addr = 0
      bootCore(dut, boot_addr)
      assertTerminates(dut)
      watchErrors(dut)

      val rd = 3

      // Test LUI
      val values = List(1, 0, 1, 2, 3, 0xfffff, 0x80000)
      var PC = boot_addr
      for (v <- values) {
        val expected = (v.toLong << 12) + PC
        executeInstruction(dut, Op.U(Instructions.AUIPC, rd, v))
        PC += 4
        assertResult(expected)(readRegister(dut, rd))
        PC += 4
      }

    }
  }

  it should "properly make jumps (JAL, JALR)." in {

    compiled.doSim { dut =>
      Random.setSeed(42)

      bootCore(dut, boot_addr = 0)

      assertTerminates(dut)

      watchErrors(dut)

      val r_link = 1
      val r_pc = 2

      // rd <- PC+4, PC <- PC + imm
      var PC = 0
      executeInstruction(dut, Op.J(Instructions.JAL, r_link, 400))
      PC += 400
      executeInstruction(dut, Op.U(Instructions.AUIPC, r_pc, 0))
      PC += 4

      assertResult(4)(readRegister(dut, r_link))
      PC += 4
      assertResult(400)(readRegister(dut, r_pc))
      PC += 4

      executeInstruction(dut, Op.J(Instructions.JAL, r_link, -212))
      PC += -4
      executeInstruction(dut, Op.U(Instructions.AUIPC, r_pc, 0))
      PC += 4

      assertResult(416)(readRegister(dut, r_link))
      assertResult(200)(readRegister(dut, r_pc))
    }
  }

  it should "properly take branches" in {

    compiled.doSim { dut =>
      Random.setSeed(42)

      bootCore(dut, boot_addr = 0)

      assertTerminates(dut)

      watchErrors(dut)

      watchDataMemoryRequests(dut)
      watchDataMemoryResponses(dut)

      // PC = 0

      val zero = 0
      val one = 1
      val two = 2
      val minusOne = 3
      val minusTwo = 4

      writeRegister(dut, one, 1)
      writeRegister(dut, two, 2)
      writeRegister(dut, minusOne, 0xffffffffl)
      writeRegister(dut, minusTwo, 0xfffffffel)

      //            assertResult(1)(readRegister(dut, one))
      //            assertResult(2)(readRegister(dut, two))
      //            assertResult(0xffffffffl)(readRegister(dut, minusOne))
      //            assertResult(0xffffffffl - 1)(readRegister(dut, minusTwo))


      // PC = 12


      val expected_addresses = (1 to 100).map(0x10 * _)

      fork {
        // Watch instruction fetch addresses.
        for (exp <- expected_addresses) {
          dut.clockDomain.waitActiveEdgeWhere(dut.io.i_req_valid_o.toBoolean & dut.io.i_req_ready_i.toBoolean)
          val addr = dut.io.i_req_addr_o.toBigInt
          println(s"Instruction fetch from: ${addr}")
          assertResult(exp, "Fetch from unexpected address. Possibly branch not properly taken.")(addr)
        }
      }


      // Branch if x0 == x0 (always the case)
      // BEQ
      executeInstruction(dut, Op.B(Instructions.BEQ, 0x10, zero, zero))
      executeInstruction(dut, Op.B(Instructions.BEQ, 0x10, zero, zero))
      executeInstruction(dut, Op.B(Instructions.BEQ, 0x10, one, one))

      //      executeInstruction(dut, Op.B(Instructions.BEQ, 0x10, two, one)) // Should fail!

      executeInstruction(dut, Op.B(Instructions.BEQ, 0x10, minusOne, minusOne))
      executeInstruction(dut, Op.B(Instructions.BEQ, 0x10, minusTwo, minusTwo))
      //            executeInstruction(dut, Op.B(Instructions.BEQ, 0x10, minusOne, minusTwo)) // Should fail!

      // BNE
      executeInstruction(dut, Op.B(Instructions.BNE, 0x10, zero, one))
      executeInstruction(dut, Op.B(Instructions.BNE, 0x10, minusOne, minusTwo))

      // BLT
      executeInstruction(dut, Op.B(Instructions.BLT, 0x10, zero, one))
      executeInstruction(dut, Op.B(Instructions.BLT, 0x10, minusTwo, minusOne))

      // BLTU
      executeInstruction(dut, Op.B(Instructions.BLTU, 0x10, zero, one))
      // As unsigned -1 == 0xffffffff is larger than 0.
      executeInstruction(dut, Op.B(Instructions.BLTU, 0x10, zero, minusOne))

      // BGE
      executeInstruction(dut, Op.B(Instructions.BGE, 0x10, zero, zero))
      executeInstruction(dut, Op.B(Instructions.BGE, 0x10, one, zero))
      executeInstruction(dut, Op.B(Instructions.BGE, 0x10, minusOne, minusOne))
      executeInstruction(dut, Op.B(Instructions.BGE, 0x10, zero, minusOne))
      executeInstruction(dut, Op.B(Instructions.BGE, 0x10, minusOne, minusTwo))

      // BGEU
      executeInstruction(dut, Op.B(Instructions.BGEU, 0x10, zero, zero))
      executeInstruction(dut, Op.B(Instructions.BGEU, 0x10, one, zero))
      executeInstruction(dut, Op.B(Instructions.BGEU, 0x10, minusOne, zero))
      executeInstruction(dut, Op.B(Instructions.BGEU, 0x10, minusOne, minusOne))
      executeInstruction(dut, Op.B(Instructions.BGEU, 0x10, minusOne, minusTwo))

      // NOP
      executeInstruction(dut, Op.R(Instructions.ADD, 0, 0, 0))

    }
  }

  it should "go to dormant mode on a WFI instruction and resume with PC according to `wakeup_resume_i`" in {

    compiled.doSim { dut =>
      Random.setSeed(42)

      bootCore(dut, boot_addr = 0)
      assertTerminates(dut)
      watchErrors(dut)

      // Test 1: resume from following instruction.
      {
        // Send the core to sleep.
        executeInstruction(dut, Instructions.WFI.value)

        var shouldBeAsleep = true
        var coreAwakeCheck1 = false
        fork {
          executeInstruction(dut, Op.I(Instructions.ANDI, 0, 0, 42))

          // This instruction should never be fetched.
          if (shouldBeAsleep) {
            assert(false, "Core should be asleep but fetched an instruction.")
          }
          coreAwakeCheck1 = true
        }

        // Wait a moment...
        dut.clockDomain.waitActiveEdge(100)

        // Wake it up!
        dut.io.wakeup_resume_i #= true
        dut.io.wakeup_i #= true
        shouldBeAsleep = false

        var coreAwakeCheck2 = false
        fork {
          // Verify the boot address.
          dut.clockDomain.waitActiveEdgeWhere(dut.io.i_req_valid_o.toBoolean)
          assertResult(4)(dut.io.i_req_addr_o.toBigInt)
          coreAwakeCheck2 = true
        }

        dut.clockDomain.waitActiveEdge()

        dut.io.wakeup_resume_i #= false
        dut.io.wakeup_i #= false

        dut.clockDomain.waitActiveEdge(20)

        assert(coreAwakeCheck1, "Core did not fetch instruction after wake-up (1).")
        assert(coreAwakeCheck2, "Core did not fetch instruction after wake-up (2).")

      }

      // Test 2: Resume from supplied address.
      {

        // Send the core to sleep.
        executeInstruction(dut, Instructions.WFI.value)

        var shouldBeAsleep = true
        fork {
          executeInstruction(dut, Op.R(Instructions.AND, 0, 0, 0))
          // This instruction should never be fetched.
          if (shouldBeAsleep) {
            assert(false, "Core should be asleep but fetched an instruction.")
          }
        }

        // Wait a moment...
        dut.clockDomain.waitActiveEdge(100)

        // Wake it up!
        dut.io.wakeup_resume_i #= false // Now this is false.
        val wakeup_addr = 0x800
        dut.io.wakeup_addr_i #= wakeup_addr
        dut.io.wakeup_i #= true
        shouldBeAsleep = false

        var coreAwakeCheck1 = false
        fork {
          // Verify the boot address.
          dut.clockDomain.waitActiveEdgeWhere(dut.io.i_req_valid_o.toBoolean)
          assertResult(wakeup_addr)(dut.io.i_req_addr_o.toBigInt)
          coreAwakeCheck1 = true
        }

        dut.clockDomain.waitActiveEdge()

        dut.io.wakeup_resume_i #= false
        dut.io.wakeup_i #= false

        dut.clockDomain.waitActiveEdge(100)

        assert(coreAwakeCheck1, "Core did not fetch instruction after wake-up.")
      }

    }
  }

  it should "MEMTEST" in {


    compiled.doSim { dut =>

      val dataMemory = mutable.HashMap[BigInt, BigInt]()
      val instructionMemory = mutable.HashMap[BigInt, BigInt]()

      def memRead(memory: mutable.HashMap[BigInt, BigInt], name: String, addr: BigInt): BigInt = {
        assertResult(0, "Unaligned read.")(addr % 4)
        val ret = memory.get(addr) match {
          case None => BigInt(0) // Memory is initialized with zeros.
          case Some(v) => v
        }
        println(s"memRead ($name): $addr -> $ret")
        ret
      }

      def memWrite(memory: mutable.HashMap[BigInt, BigInt], name: String, addr: BigInt, value: BigInt) = {
        assertResult(0, "Unaligned write.")(addr % 4)
        println(s"memWrite ($name): $addr <- $value")
        memory(addr) = value
      }

      bootCore(dut, boot_addr = 0x0)

      // Create data memory controller.
      memControl(
        dut.clockDomain,
        dut.io.d_req_ready_i,
        dut.io.d_req_valid_o,
        Some(dut.io.d_req_wstrb_o),
        Some(dut.io.d_req_wdata_o),
        dut.io.d_req_addr_o,
        dut.io.d_rsp_ready_o,
        dut.io.d_rsp_valid_i,
        dut.io.d_rsp_rdata_i,
        memRead(dataMemory, "data", _),
        memWrite(dataMemory, "data", _, _)
      )

      // Create instruction memory controller.
      memControl(
        dut.clockDomain,
        dut.io.i_req_ready_i,
        dut.io.i_req_valid_o,
        None,
        None,
        dut.io.i_req_addr_o,
        dut.io.i_rsp_ready_o,
        dut.io.i_rsp_valid_i,
        dut.io.i_rsp_data_i,
        memRead(instructionMemory, "instr", _),
        (a, b) => {}
      )

      import Instructions._
      val program: List[BigInt] = List(
        Op.I(ADDI, 1, 0, 42),
        Op.S(Instructions.SW, 0x00, 0, 1),
        Op.S(Instructions.SW, 0x00, 0, 1),
        WFI.value
      )

      // Write program to instruction memory.
      for ((instr, addr) <- program.zipWithIndex) {
        instructionMemory(addr * 4) = instr
      }


      dut.clockDomain.waitActiveEdge(100)
    }

  }
}
